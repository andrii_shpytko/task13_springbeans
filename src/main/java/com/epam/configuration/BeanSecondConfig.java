package com.epam.configuration;

import com.epam.beans.BeanB;
import com.epam.beans.BeanC;
import com.epam.beans.BeanD;
import com.epam.beans.BeanF;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("bean.properties")
public class BeanSecondConfig {

    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;

    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;

    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;

    @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value="beanD")
    public BeanB getBeanB() {
        return new BeanB(nameB, valueB);
    }

    @Bean(value = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value="beanB")
    public BeanC getBeanC() {
        return new BeanC(nameC, valueC);
    }

    @Bean(value = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD(nameD, valueD);
    }

    @Bean("beanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }
}
