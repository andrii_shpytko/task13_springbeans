package com.epam.configuration;

import com.epam.beans.*;
import com.epam.beans.validator.CustomBeanBInitBeanFactoryPostProcessor;
import com.epam.beans.validator.ValidateBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({BeanSecondConfig.class})
public class BeanFirstConfig {
    @Bean("firstBeanA")
    public BeanA getFirstBeanA(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getName(), beanC.getValue());
    }

    @Bean("secondBeanA")
    public BeanA getSecondBeanA(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getName(), beanD.getValue());
    }

    @Bean("thirdBeanA")
    public BeanA getThirdBeanA(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC.getName(), beanD.getValue());
    }

    @Bean("firstBeanE")
    public BeanE getFirstBeanE(BeanA firstBeanA) {
        return new BeanE(firstBeanA.getName(), firstBeanA.getValue());
    }

    @Bean("secondBeanE")
    public BeanE getSecondBeanE(BeanA secondBeanA) {
        return new BeanE(secondBeanA.getName(), secondBeanA.getValue());
    }

    @Bean("thirdBeanE")
    public BeanE getThirdBeanE(BeanA thirdBeanA) {
        return new BeanE(thirdBeanA.getName(), thirdBeanA.getValue());
    }

    @Bean(value = "changeMethod", initMethod = "init")
    public CustomBeanBInitBeanFactoryPostProcessor changeMethod() {
        return new CustomBeanBInitBeanFactoryPostProcessor();
    }
    @Bean
    public ValidateBeanPostProcessor validateBeanPostProcessor(){
        return new ValidateBeanPostProcessor();
    }
}
