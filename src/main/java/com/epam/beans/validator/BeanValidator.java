package com.epam.beans.validator;

@FunctionalInterface
public interface BeanValidator {
    void validate();
}
