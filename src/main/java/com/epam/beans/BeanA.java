package com.epam.beans;

import com.epam.beans.validator.BeanValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;

    public BeanA() {
    }

    public BeanA(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("BeanA afterPropertySet method [initializingBean] " + this);
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("BeanA destroy method [DisposableBean] " + this);
    }
}
