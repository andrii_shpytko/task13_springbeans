package com.epam.beans;

import com.epam.beans.validator.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements BeanValidator {
    private String name;
    private int value;

    public BeanE() {
    }

    public BeanE(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {

    }

    @PostConstruct
    public void postConstructMethod() {
        System.out.println("BeanE [@PostConstruct] " + this);
    }

    @PreDestroy
    public void preDestroyMethod() {
        System.out.println("BeanE [@PreDestroy] " + this);
    }
}
