package com.epam.beans;

import com.epam.beans.validator.BeanValidator;

public class BeanB implements BeanValidator {
    private String name;
    private int value;

    public BeanB() {
    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate() {

    }

    private void init() {
        System.out.println("BeanB init method " + this);
    }

    private void destroy() {
        System.out.println("BeanB destroy method " + this);
    }
}
