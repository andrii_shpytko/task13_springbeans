package com.epam;

import com.epam.configuration.BeanFirstConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class Appl {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext();
        ((AnnotationConfigApplicationContext) context).register(BeanFirstConfig.class);
        ((AnnotationConfigApplicationContext) context).refresh();
        Arrays.stream(context.getBeanDefinitionNames())
                .forEach(System.out::println);
        ((AnnotationConfigApplicationContext) context).close();
    }
}
